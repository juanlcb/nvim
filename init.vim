" Vim-plug installs
call plug#begin()
" The default plugin directory will be as follows:
"   - Vim (linux/macOS): '~/.vim/plugged'
"   - Vim (Windows): '~/vimfiles/plugged'
"   - Neovim (linux/macOS/Windows): stdpath('data') . '/plugged'
" You can specify a custom plugin directory by passing it as the argument
"   - e.g. `call plug#begin('~/.vim/plugged')`
"   - Avoid using standard Vim directory names like 'plugin'
"
" Make sure you use single quotes when loading plugins
" Shorthand notation; fetches https://github.com/junegunn/vim-easy-align
"Plug 'junegunn/vim-easy-align'
" Any valid git URl is also allowed
"Plug 'https://github.com/junegunn/vim-github-dashboard.git'
"
" Plugs currently being loaded
Plug 'preservim/nerdtree'
Plug 'junegunn/goyo.vim'
Plug 'skywind3000/asyncrun.vim'
Plug 'conornewton/vim-pandoc-markdown-preview'
Plug 'godlygeek/tabular'
Plug 'preservim/vim-markdown'
Plug 'vimwiki/vimwiki'
Plug 'joshdick/onedark.vim'
"Plug 'sainnhe/everforest'
"Plug 'crusoexia/vim-monokai'
" After installing, just run :PlugInstall
" Initialize plugin system
call plug#end()

" Basic configuration and variables
" Color Schemes that I've liked:
"    colorscheme torte
"    colorscheme elflord
"    colorscheme murphy
colorscheme onedark
set matchpairs+=«:»
set nu rnu
set splitright
set splitbelow
set foldmethod=manual
set timeoutlen=2000
let g:mapleader = " "
let g:maplocalleader = "-"

" Configure vimwiki directory
"" This sets all of my MD files to type vimwiki
"" That's why I've maped the filetype change for markdown
set nocompatible
filetype plugin on
syntax on 

let g:vimwiki_list = [{'path': '~/documentos/vimwiki/',
                      \ 'syntax': 'markdown', 'ext': '.md'}]

function! VimwikiLinkHandler(link)
    " Use Vim to open external files with the 'vfile:' scheme.  E.g.:
    "   1) [[vfile:~/Code/PythonProject/abc123.py]]
    "   2) [[vfile:./|Wiki Home]]
    let link = a:link
    if link =~# '^vfile:'
      let link = link[1:]
    else
      return 0
    endif
    let link_infos = vimwiki#base#resolve_link(link)
    if link_infos.filename == ''
      echomsg 'Vimwiki Error: Unable to resolve link!'
      return 0
    else
      exe 'tabnew ' . fnameescape(link_infos.filename)
      return 1
    endif
  endfunction

" Configure markdown folding
set nocompatible
if has("autocmd")
	filetype plugin indent on
endif

"" This one is for vim-pandoc-markdown-preview
let g:md_pdf_viewer="zathura"

"" This one assigns the header level for folds in markdown
let g:vim_markdown_folding_level = 1

"" This one makes sure that not all .md
"" files are set as vimwiki's syntax
let g:vimwiki_global_ext=0

" This is the cordcount
func! WordCount()
  let wc = wordcount()
  return ''.(has_key(wc, 'visual_words') ? wc.visual_words : wc.cursor_words).'/'.wc.words.' p:'.(has_key(wc, 'cursor_words') ? 1 + wc.cursor_words/350.'/' : '').(1 + wc.words/350)
endfunc
" let g:word_count=wordcount().words
" function WordCount()
"     if has_key(wordcount(),'visual_words')
"         let g:word_count=wordcount().visual_words."/".wordcount().words " count selected words
"     else
"         let g:word_count=wordcount().cursor_words."/".wordcount().words " or shows words 'so far'
"     endif
"     return g:word_count
" endfunction

set statusline+=\ w:%{WordCount()}
set laststatus=2 " show the statusline

" Basic commands mapped with leader key
nmap <leader>ss :setlocal spell! spelllang=es<CR>
nmap <leader>sc :setlocal spell! spelllang=ca_es<CR>
nmap <leader>se :setlocal spell! spelllang=en_us<CR>

" Manipulating folds from vim-markdown plugin
nmap <Tab> za
nmap <S-Tab> zc
nmap <leader><Tab> zA
nmap <leader><S-Tab> zC
nmap <localleader><Tab> zR
nmap <localleader><S-Tab> zM

" Insert spaces while remaining in normal mode
nmap <leader>o o<Esc>
nmap <leader>O O<Esc>

" Copy and paste to and from clipboard
vnoremap <leader>y "+y
nnoremap <leader>p "+p

" Character count
nmap <localleader>cn :w !echo -n "Total characters (no white space): " && wc -m<CR>
nmap <localleader>cc :w !echo -n "Total characters (with white space): " && wc -c<CR>
nmap <localleader>wc :w !echo -n "Total words: " && wc -w<CR>

" Buffers and explorer
nmap <leader>bn :bn<CR>
nmap <leader>bp :bp <CR> 
nmap <leader>ex :Ex<CR> 
nmap <leader>ls :ls<CR>

" Plugin modes mapped
nmap <leader>mp :StartMdPreview<CR>
nmap <localleader>mp :StopMdPreview<CR>
nmap <leader>mm :setlocal filetype=markdown<CR> 
nmap <leader>mw :setlocal filetype=vimwiki<CR> 
nmap <leader>f :Goyo<CR>
nmap <localleader>f :Goyo!<CR>
nmap <leader>ws <Plug>VimwikiVSplitlink
nmap <C-n> :NERDTreeToggle<CR>

" Add markdown commets (not processed by pandoc) and insert 10 dashes
nmap <leader>co i<!--- 
nmap <leader>ce a---><CR><Esc>

" Add spacing bar in markdown
nmap <leader>cl 20i-<Esc>

" Window splits
nmap <localleader>hs :sp<CR>
nmap <localleader>vs :vsp<CR>

" Remapped movement keys
nmap <Leader>h <C-w>h
nmap <Leader>j <C-w>j
nmap <Leader>k <C-w>k
nmap <Leader>l <C-w>l
nmap <Localleader>j <C-w><
nmap <Localleader>k <C-w>>
nmap <Localleader>u <C-w>-
nmap <Localleader>i <C-w>+
nmap ñ ^
nmap ç $
imap <C-z> «
imap <C-x> »
imap <C-a> —
imap <C-.>, <C-y>,

" DEPRICATED

"" I've set longer timeoutlen for complex markdown keybinding
""nmap <leader>mm :setlocal filetype=markdown<CR> 
""nmap <leader>mw :setlocal filetype=vimwiki<CR> 

"" Start NERDTree when Vim is started without file arguments.
""autocmd StdinReadPre * let s:std_in=1
""autocmd VimEnter * if argc() == 0 && !exists('s:std_in') | NERDTree | endif

"" Word count
""map <leader>wc :w !wc -w<CR>

"" Word count live
"" let g:word_count=wordcount().words
"" function WordCount()
""     if has_key(wordcount(),'visual_words')
""         let g:word_count=wordcount().visual_words."/".wordcount().words " count selected words
""     else
""         let g:word_count=wordcount().cursor_words."/".wordcount().words " or shows words 'so far'
""     endif
""     return g:word_count
"" endfunction
"" set statusline+=\ w:%{WordCount()},
"" set laststatus=2 " show the statusline
"" End of word count

